/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.woodproblem.domain.solver.WoodBeamStrengthComparator;
import org.optaplanner.examples.woodproblem.domain.solver.WoodMeasureDifficultyComparator;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@PlanningEntity(difficultyComparatorClass = WoodMeasureDifficultyComparator.class)
@XStreamAlias("WoodMeasure")
public class WoodMeasure extends AbstractPersistable {

	private static final long serialVersionUID = 1L;

	private int requiredLength; // necessary length of beam

    // Planning variables: changes during planning, between score calculations.
    private WoodBeam beam;

    public WoodMeasure() {
	}
    
    public WoodMeasure(long id, int requiredLength) {
    	this();
    	setId(id);
    	setRequiredLength(requiredLength);
    }
    
    public int getRequiredLength() {
        return requiredLength;
    }

    public void setRequiredLength(int requiredLength) {
        this.requiredLength = requiredLength;
    }

    @PlanningVariable(valueRangeProviderRefs = {"beamRange"}, strengthComparatorClass = WoodBeamStrengthComparator.class)
    public WoodBeam getBeam() {
        return beam;
    }

    public void setBeam(WoodBeam beam) {
        this.beam = beam;
    }

    public String getLabel() {
        return "Wood Measure of size " + requiredLength + "mm";
    }

    @Override
    public String toString() {
        return super.toString() + "@" + beam;
    }

}
