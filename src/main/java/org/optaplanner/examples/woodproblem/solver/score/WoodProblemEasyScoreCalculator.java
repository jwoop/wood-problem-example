/*
 * Copyright 2012 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.solver.score;

import java.util.HashSet;
import java.util.Set;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodProblemEasyScoreCalculator implements EasyScoreCalculator<WoodAssignment> {

    /**
     * A very simple implementation. The double loop can easily be removed by using Maps as shown in
     * {@link WoodProblemMapBasedEasyScoreCalculator#calculateScore(WoodAssignment)}.
     */
    public HardSoftScore calculateScore(WoodAssignment woodAssignment) {
        int hardScore = 0;
        int softScore = 0;
        Set<WoodBeam> usedBeams = new HashSet<WoodBeam>();
        for (WoodBeam beam : woodAssignment.getBeamList()) {
            int beamLengthUsage = 0;

            // Calculate usage
            for (WoodMeasure measure : woodAssignment.getMeasureList()) {
                if (beam.equals(measure.getBeam())) {
                    beamLengthUsage += measure.getRequiredLength();
                }
            }

            // Hard constraints
            int beamLengthAvailable = beam.getLength() - beamLengthUsage;
            if (beamLengthAvailable < 0) {
                hardScore += beamLengthAvailable;
            }
            usedBeams.add(beam);
        }
        // Soft constraints
        softScore = usedBeams.size();
        
        return HardSoftScore.valueOf(hardScore, softScore);
    }

}
