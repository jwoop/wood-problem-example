package org.optaplanner.examples.woodproblem.solver.move;

import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodProblemMoveHelper {

    public static void moveWoodBeam(ScoreDirector scoreDirector, WoodMeasure measure,
            WoodBeam toBeam) {
        scoreDirector.beforeVariableChanged(measure, "beam");
        measure.setBeam(toBeam);
        scoreDirector.afterVariableChanged(measure, "beam");
    }

    private WoodProblemMoveHelper() {
    }

}
