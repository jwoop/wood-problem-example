package org.optaplanner.examples.woodproblem.solver.move.factory;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;
import org.optaplanner.examples.woodproblem.solver.move.WoodBeamChangeMove;

public class WoodBeamChangeMoveFactory implements MoveListFactory<WoodAssignment> {

    public List<Move> createMoveList(WoodAssignment assignment) {
        List<Move> moveList = new ArrayList<Move>();
        List<WoodBeam> beamList = assignment.getBeamList();
        for (WoodMeasure measure : assignment.getMeasureList()) {
            for (WoodBeam beam : beamList) {
                moveList.add(new WoodBeamChangeMove(measure, beam));
            }
        }
        return moveList;
    }

}
