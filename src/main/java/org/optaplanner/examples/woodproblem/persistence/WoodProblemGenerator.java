/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.persistence;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.optaplanner.examples.common.app.LoggingMain;
import org.optaplanner.examples.common.persistence.SolutionDao;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodProblemGenerator extends LoggingMain {

    public static void main(String[] args) {
        new WoodProblemGenerator().generate();
    }

    protected final SolutionDao solutionDao;
    protected final File outputDir;
    protected Random random;

    public WoodProblemGenerator() {
        solutionDao = new WoodProblemDao();
        outputDir = new File(solutionDao.getDataDir(), "unsolved");
    }

    public WoodProblemGenerator(boolean withoutDao) {
        if (!withoutDao) {
            throw new IllegalArgumentException("The parameter withoutDao (" + withoutDao + ") must be true.");
        }
        solutionDao = null;
        outputDir = null;
    }

    public void generate() {
        writeWoodProblem(8, 1, 2440);
        writeWoodProblem(8, 1, 3050);
        writeWoodProblem(8, 1, 3660);
        writeWoodProblem(8, 1, 4270);
    }
    
    private void writeWoodProblem(int smallBoxAmount, int bigBoxAmount, int beamLength) {
    	WoodAssignment problem = createWoodProblem(smallBoxAmount, bigBoxAmount, beamLength);
    	String inputId = "" + beamLength + "beams-with" + smallBoxAmount 
    			+ "SmallAnd" + bigBoxAmount + "BigBoxes";
    	File outputFile = new File(outputDir, inputId + ".xml");
    	if (!outputFile.exists()) {
    		try {
    			outputFile.createNewFile();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	solutionDao.writeSolution(problem, outputFile);
    }

    public WoodAssignment createWoodProblem(int smallBoxesSize, int bigBoxesSize, int beamLength) {
    	List<WoodMeasure> measureList = new ArrayList<WoodMeasure>();
    	long id = 1;
    	for (int index = 0; index < smallBoxesSize; index++) {
    		for (int i = 0; i < 5; i++) {
    			measureList.add(new WoodMeasure(id++, 500));
    		}
    		for (int i = 0; i < 4; i++) {
    			measureList.add(new WoodMeasure(id++, 473));
    		}
    		for (int i = 0; i < 8; i++) {
    			measureList.add(new WoodMeasure(id++, 50));
    		}
    	}
    	for (int index = 0; index < bigBoxesSize; index++) {
    		for (int i = 0; i < 10; i++) {
    			measureList.add(new WoodMeasure(id++, 1000));
    		}
    		for (int i = 0; i < 8; i++) {
    			measureList.add(new WoodMeasure(id++, 973));
    		}
    		for (int i = 0; i < 12; i++) {
    			measureList.add(new WoodMeasure(id++, 50));
    		}
    	}
    	List<WoodBeam> beamList = new ArrayList<WoodBeam>();
    	//worst case scenario, we use one beam per measure. That's our initial scenario
    	for (int index = 0; index < measureList.size(); index++) {
    		beamList.add(new WoodBeam(id++, beamLength));
    	}
    	
    	WoodAssignment assignment = new WoodAssignment();
    	assignment.setBeamList(beamList);
    	assignment.setMeasureList(measureList);
    	assignment.setId(0l);
    	return assignment;
    }
}
